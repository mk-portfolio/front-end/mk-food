/* ===== Mobile Navigation Start ===== */
const btnNavEl = document.querySelector(".btn-mobile-nav");
const headerEl = document.querySelector(".header");

btnNavEl.addEventListener('click', function(){

	headerEl.classList.toggle("nav-open");

});
/* ===== Mobile Navigation End   ===== */





/* ===== Smooth Scrolling Animation Start ===== */
const allLinks = document.querySelectorAll('a:link');
// to check console.log(allLinks);

allLinks.forEach(link => {

	link.addEventListener('click', e => {

		//to check console.log(e);
		e.preventDefault();

		// Store all the href attribute
		const href = link.getAttribute('href');
		// console.log(href);

		// Scroll-Back-to-TOP
		if (href === '#') {

			window.scrollTo({
				top: 0,
				behavior: 'smooth'
			});
		}

		// Scroll To Other Links
		if (href !== '#' && href.startsWith('#')) {

			// Store all the "Section Element" that has an href attribute
			const sectionEl = document.querySelector(href);

			sectionEl.scrollIntoView({behavior: 'smooth'});
		}

		// Close mobile Navigation
		if(link.classList.contains('main-nav-link')){
			headerEl.classList.toggle("nav-open");
		}
	});
});
/* ===== Smooth Scrolling Animation End   ===== */





/* ===== Sticky Navigation Start ===== */
// Make The sticky Nav once the hero section is not in the viewport

const sectionHeroEl = document.querySelector(".section-hero");

const observer = new IntersectionObserver( entries => {

		const ent = entries[0];
		// console.log(ent);

		!ent.isIntersecting ? document.body.classList.add('sticky') : document.body.classList.remove('sticky');

	},
	{
		// In the ViewPort
		root: null,
		threshold: 0,
		rootMargin: '-80px'
	}
);

observer.observe(sectionHeroEl);
/* ===== Sticky Navigation End   ===== */